@extends('adminlte::page')
@section('content_header')
    <h1>Profiles</h1>
@stop

@section('content')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><b>User List</b> </h3>

          <div class="card-tools">
            <div class="input-group input-group-sm" style="width: 150px;">
                <a href="{{route('user.create')}}" role="button" class="btn btn-block bg-gradient-success">Add New User</a>
            </div>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0 ">
          <table class="table table-hover text-nowrap table-sm table-responsive-sm">
            <thead>
              <tr class="">
                <th>Foto</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Telefono</th>
                <th>Dirección</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
               @if ($users ?? '' )


                @foreach ($users as $user)


              <tr>
                <td > @if ($user->img)
                    <img src="/img/{{$user->img}}" class="profile-user-img img-fluid img-circle img-bordered-sm">
                    @else
                    <img src="/img/perfil.png" class="profile-user-img img-fluid img-circle">
                    @endif</td>
                <td><a href="{{route('user.show', $user->id)}}" >{{ $user->name }}</a></td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->address }}</td>
                <td>
                    <div class="btn-group btn-group-xs" role="group" aria-label="...">
                        {!! Form::model($user,['method'=>'DELETE', 'action'=>['UsersController@destroy',$user->id]]) !!}
                        <a class="btn btn-primary " href="{{route('user.edit', $user->id)}}"><i class="fas fa-edit"></i></a>
                        {!! Form::button('<i class="fas fa-trash"></i>', ['class'=>'btn btn-danger', 'type'=>'submit']) !!}

                        {!! Form::close() !!}
                    </div>


                </td>
            @endforeach
            @endif


                </tr>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop
