@extends('adminlte::page')
@section('content_header')


<div class="row justify-content-md-center">
    <div class="col-md-6">


<div class="card card-primary card-outline">
    <div class="card-body box-profile">
      <div class="row" >
         
          
        <div class=" col-md-3 text-center">
            <img class="profile-user-img img-fluid img-circle" src="https://picsum.photos/300/300" alt="User profile picture">
            
        </div>
        <div class=" col-md-6">
        <h3 class="profile-username text-center">{{ $users->name }}</h3>
      
        <p class="text-muted text-center">{{ $users->email }}</p> 
    </div>
    </div>



     
      <div class="row justify-content-md-center">
        <div class="col-md-3">
      <strong><i class="fas fa-map-marker-alt mr-1"></i> Dirección</strong>
      <p class="text-muted">{{ $users->address}}</p>
    </div>
    <div class="col-md-3">
        <strong><i class="fas fa-map-marker-alt mr-1"></i> Teléfono</strong>
        <p class="text-muted">{{ $users->phone}}</p>
    </div>
    </div>
      <a href="{{route('user.edit', $user->id)}}" class="btn btn-primary "><b>Edit</b></a>
    </div>
    <!-- /.card-body -->
  </div>
</div>
</div>
@stop