@extends('adminlte::page')

@section('content_header')
    <h1>Profiles</h1>
@stop

@section('content')

{!! Form::model($users, ['method' => 'PATCH', 'action'=>['UsersController@update',$users->id]]) !!}

<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Create User</h3>
    </div>
     
      <div class="card-body">
          <div class="row">
               
                
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('name', 'Nombre') !!}
                        {!! Form::text('name',null, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : null), 'placeholder'=> "Nombre"]) !!}
                    @if($errors->has('name'))
                        <div class="invalid-feedback">
                            <strong>El campo de nombre es obligatorio.</strong>
                        </div>
                    @endif
                  
                    </div> 
                   
                    <div class="form-group">
                        {!! Form::label('phone', 'Teléfono') !!}
                        {!! Form::text('phone',null, ['class' => 'form-control', 'placeholder'=> "Teléfono"]) !!}
                       
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        {!! Form::label('email', 'Email') !!}
                        {!! Form::text('email',null, ['class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : null), 'placeholder'=> "Email"]) !!}
                        @if($errors->has('name'))
                        <div class="invalid-feedback">
                            <strong>El campo de Email es obligatorio.</strong>
                        </div>
                        @endif
                    </div> 
                    
                    <div class="form-group">
                        {!! Form::label('address', 'Dirección') !!}
                        {!! Form::text('address',null, ['class' => 'form-control', 'placeholder'=> "Dirección"]) !!}
                       
                    </div> 
                </div>
          </div>   
        </div>
      

     
      <!-- /.card-body -->

      <div class="card-footer">
        {!! Form::submit('Actualizar Usuario', ['class'=>'btn btn-primary']) !!}
      </div>
     
</div>
{!! Form::close() !!}
@stop
@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    
@stop